import { useState, useEffect } from "react";
import "./App.css";

function App() {
  // Declare constants
  const [todos, setTodos] = useState([]);
  const [todo, setTodo] = useState("");

  // Get Data
  useEffect(() => {
    getTodos();
  }, []);

  const getTodos = async () => {
    let data = await fetch("http://127.0.0.1:8000/", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    });

    data = await data.json();

    setTodos(data);
  };

  // Add new todo
  const addTodo = async () => {
    await fetch("http://127.0.0.1:8000/add", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ text: todo }),
    });

    getTodos();
  };

  // Delete existing todo
  const deleteTodo = async (id) => {
    await fetch("http://127.0.0.1:8000/remove?id=" + id, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
    });

    getTodos();
  };

  // Check todo
  const completeTodo = async (id, val) => {
    let newTodos = [...todos];
    let todoState;

    newTodos.map((todo) => {
      if (todo.id === id) {
        todo.completed = !todo.completed;
        todoState = todo.completed;
      }
    });

    todoState = todoState ? 1 : 0;

    await fetch("http://127.0.0.1:8000/update", {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ id, completed: todoState }),
    });

    setTodos(newTodos);
  };

  return (
    <div className="App">
      <header className="App-header">
        <h1>Todo App</h1>
        <ul>
          {todos.length > 0
            ? todos.map((todo, idx) => {
                return (
                  <li key={idx}>
                    <input
                      type="checkbox"
                      onChange={(e) => completeTodo(todo.id, e.target.value)}
                      checked={todo.completed}
                    />
                    <p>{todo.text}</p>
                    <button onClick={() => deleteTodo(todo.id)}>Borrar</button>
                  </li>
                );
              })
            : "Sin tareas"}
        </ul>

        <input
          type="text"
          placeholder="Add todo"
          value={todo}
          onChange={(e) => setTodo(e.target.value)}
        />
        <button onClick={() => addTodo()}>Add todo</button>
      </header>
    </div>
  );
}

export default App;
