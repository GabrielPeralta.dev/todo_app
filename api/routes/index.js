var express = require("express");
var router = express.Router();

// get the client
const mysql = require("mysql2");

// create the connection to database
const pool = mysql.createPool({
  host: "localhost",
  user: "root",
  password: "",
  database: "todo_app",
});

router.get("/", async (req, res) => {
  pool.query(`select * from todos`, (err, results, fields) => {
    results.forEach((resp) => {
      if (resp.completed == 0) {
        resp.completed = false;
      } else {
        resp.completed = true;
      }
    });
    res.status(200).json(results);
  });
});

router.post("/add", async (req, res) => {
  let { text } = req.body;
  pool.query(
    `insert into todos (text) values ('${text}')`,
    (err, results, fields) => {
      console.log(results);
      res.status(200).json(results);
    }
  );
});

router.put("/update", async (req, res) => {
  let { id, completed } = req.body;

  console.log(id, completed);

  pool.query(
    `update todos set completed = ${completed} where id = ${id}`,
    (err, results, fields) => {
      res.status(200).json(results);
    }
  );
});

router.delete("/remove", async (req, res) => {
  let { id } = req.query;
  pool.query(`delete from todos where id = ${id}`, (err, results, fields) => {
    res.status(200).json(results);
  });
});

module.exports = router;
